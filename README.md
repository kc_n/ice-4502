# Report

1. **Game Development**
The first part of this project was to decide on a game and programme it on the Microbit. To develop ideas, I first came up with 8 designs and then refined them down to one. My chosen design was a Space Invaders game. I used the [Microbit makecode editor](https://makecode.microbit.org/#editor) to programme the game

2. **Battery Pack** - custom PCB design
I designed a custom PCB to hold a battery and a switch to turn the microbit on/off. I designed the PCB in SVG format using [Affinity Designer](https://affinity.serif.com/en-gb/designer/).
Components for custom PCB:
- AYZ0102AGRLC Slide Switch [Link](https://uk.farnell.com/c-k-components/ayz0102agrlc/switch-spdt-0-1a-12vdc-smd-on/dp/2319971)
- CH28-2032LF Coin Cell Battery Holder [Link](https://uk.farnell.com/multicomp/ch28-2032lf/battery-holder-smt-20mm/dp/2064725?st=2032%20holder)

3. **Enclosure**
Because I chose Space Invaders as my game for this project, i wanted to give the enclosure a retro game design, so i chose to design the enclosure in an arcade machine shape. 
The software I used to create the enclosure design was [Shapr3d](https://www.shapr3d.com). In my initial design the top slid off the enclosure so that the Microbit and PCB could be inserted, however I realised that due to the buttons sticking out on the front of the Microbit this design would not work. So in my final design both the top and front of the enclosure can be removed to place the Microbit (and PCB) inside. This modification also allows the buttons to stick out of the enclosure so that they are easier to press.
The enclosure was printed on a 3D printer, and then the edges and tabs were sanded to ensure that it closed securely.

4. **Packaging**
I created the box design using software called MacDraft Pro. I wanted the box to have a unique design, so I tried to create it in a space invader character shape. For the box design, red lines were used to indicate cuts, and blue was used to indicate scores.
The box was cut on a laser cutter from 2mm corrugated cardboard.  


5. **Labelling**
I chose to create some simple space invader character sticker designs for the product.


6. **Future Developments**
Connect separate button and joystick to control the microbit, make holes in the enclosure so that the controls fit onto the arcade machine. I would also transfer my design onto the enclosure, and have a similar design on the box/packaging.
